﻿using System;
using System.Collections.Generic;

namespace Sets
{
    public class Set
    {
        private List<int> Data { get; set; }

        public Set()
        {
            Data = new List<int>();
        }

        public int Size()
        {
            return Data.Count;
        }

        public void Add(int elem)
        {
            if (!IsExist(elem))
                Data.Add(elem);
        }

        public void Remove(int elem)
        {
            Data.Remove(elem);
        }

        public IEnumerable<int> GetData()
        {
            return Data;
        }

        public bool IsExist(int number)
        {
            return Data.Contains(number);
        }

        public Set Union(Set uSet)
        {
            var set = new Set();

            return set;
        }

        public Set Intersection(Set iSet)
        {
            var set = new Set();
            foreach (var elem in Data)
            {
                if (iSet.IsExist(elem))
                {
                    set.Add(elem);
                }
            }

            return set;
        }

        public Set Difference(Set dSet)
        {
            var set = new Set();
            foreach (var elem in Data)
            {
                set.Add(elem);
            }

            foreach (var elem in dSet.Data)
            {
                set.Remove(elem);
            }

            set = (Set) MemberwiseClone();
            return set;
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }

    internal static class Program
    {
        private static void Show(IEnumerable<int> data, string prefix)
        {
            Console.Write(prefix + ": ");
            foreach (var elem in data)
            {
                Console.Write(elem + " ");
            }

            Console.WriteLine();
        }

        public static void Main(string[] args)
        {
            var f = new Set();
            var s = new Set();
            var rnd = new Random();
            for (var i = 0; i < 4; i++)
            {
                f.Add(rnd.Next(10));
                s.Add(rnd.Next(10));
            }

            Show(f.GetData(), "First");
            Show(s.GetData(), "Second");
            Show(f.Union(s).GetData(), "Union");
            Show(f.Difference(s).GetData(), "Difference");
            Show(f.Intersection(s).GetData(), "Intersection");
        }
    }
}